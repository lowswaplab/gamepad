
// HTML5 Gamepad

// https://gitlab.com/lowswaplab/gamepad

// Copyright © 2021-2022 Low SWaP Lab lowswaplab.com

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program.  If not, see
// <https://www.gnu.org/licenses/>.


//
// X config
//

// 3Dconnexion SpaceMouse Wireless (via USB cable)
// 3Dconnexion SpaceMouse Wireless (via wireless receiver)
// Microsoft X-Box One controller (via USB cable)
//
// /usr/share/X11/xorg.conf.d/50-joystick.conf
//  Section "InputClass"
//          Identifier "joystick catchall"
//          MatchIsJoystick "on"
//          MatchDevicePath "/dev/input/event*"
//          Driver "joystick"
//          Option "StartKeysEnabled" "False"
//          Option "StartMouseEnabled" "False"
//  EndSection

//
// xinput
//
// (not necessary if using the X config above)

// Microsoft X-Box One controller (via Bluetooth)
//
// XXX this isn't enough... doesn't work
// echo 1 > /sys/module/bluetooth/parameters/disable_ertm
// xinput --set-prop "Xbox Wireless Controller" "Axis Type" 0 0 0 0 0 0 0 0
// xinput --set-prop "Xbox Wireless Controller" "Button Number" 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5

// Microsoft X-Box One controller (via USB cable)
//
// xinput --list-props "Microsoft X-Box One S pad"
// xinput --set-prop "Microsoft X-Box One S pad" "Axis Type" 0 0 0 0 0 0 0 0
// xinput --set-prop "Microsoft X-Box One S pad" "Button Number" 5 5 5 5 5 5 5 5 5 5 5

// 3Dconnexion SpaceMouse Wireless (via wireless receiver)
//
// xinput --list-props "3Dconnexion 3Dconnexion Universal Receiver"
// xinput --set-prop "3Dconnexion 3Dconnexion Universal Receiver" "Axis Type" 0 0 0 0 0 0
// xinput --set-prop "3Dconnexion 3Dconnexion Universal Receiver" "Button Number" 5 5

// 3Dconnexion SpaceMouse Wireless (via USB cable)
//
// xinput --list-props "3Dconnexion SpaceMouse Wireless"
// xinput --set-prop "3Dconnexion SpaceMouse Wireless" "Axis Type" 0 0 0 0 0 0
// xinput --set-prop "3Dconnexion SpaceMouse Wireless" "Button Number" 5 5

// https://developer.mozilla.org/en-US/docs/Web/API/Gamepad_API/Using_the_Gamepad_API

"use strict";

export function Gamepad()
  {
  let debug=false;
  let focusonly=true;
  let isfocus=true;
  let sensitivity = 0.5;
  let clickTimeout = 250;

  if (debug) console.log("Gamepad()");

  const controllers={};

  this.setClickTimeout = function(_timeout)
    {
    if (_timeout >= 0)
      clickTimeout = _timeout;
    };

  this.setDebug = function(_debug)
    {
    if (_debug === true)
      debug = true;
    };

  this.setFocusOnly = function(value)
    {
    focusonly = false;
    if (value === true)
      focusonly = true;
    };

  this.setSensitivity = function(value)
    {
    if (value < 0 || value > 1)
      return(false);

    sensitivity = value;

    return(true);
    };

  const connecthandler = function(evt)
    {
    console.log("Connected", evt.gamepad.id);
    addgamepad(evt.gamepad);
    };

  const disconnecthandler = function(evt)
    {
    console.log("Disconnected", evt.gamepad.id);
    removegamepad(evt.gamepad);
    };

  const addgamepad = function(gamepad)
    {
    console.log("Add gamepad", gamepad.id);
    controllers[gamepad.index] = gamepad;
    requestAnimationFrame(updateStatus);
    };

  const removegamepad = function(gamepad)
    {
    console.log("Delete gamepad", gamepad.id);
    delete controllers[gamepad.index];
    };

  const scangamepads = function()
    {
    let gamepads;

    if (debug) console.dir("Gamepad.scangamepads()");

    gamepads = navigator.getGamepads ? navigator.getGamepads() :
      (navigator.webkitGetGamepads ? navigator.webkitGetGamepads() : []);

    for (let i = 0; i < gamepads.length; i++)
      {
      if (gamepads[i])
        {
        if (gamepads[i].index in controllers)
          controllers[gamepads[i].index] = gamepads[i];
        else
          addgamepad(gamepads[i]);
        };
      };

    requestAnimationFrame(updateStatus);
    };

  const dispatchEvent = function(id, name, value)
    {
    let evt = new Event(name);

    if (debug) console.log("Gamepad.dispatchEvent():", id, name, value);

    evt.id = id;
    evt.value = value;

    window.dispatchEvent(evt);
    };

  // TODO Double, triple click
  const checkButton = function(button, conID, conName, butName)
    {
    if (button.pressed)
      {
      if (!button.time)
        button.time = Date.now();
      dispatchEvent(conID, conName + "Down" + butName, 1);
      }
    else if (button.time)
      {
      dispatchEvent(conID, conName + "Up" + butName, 1);
      if (Date.now() - button.time <= clickTimeout)
        dispatchEvent(conID, conName + "Click" + butName, 1);
      button.time = 0;
      };
    };

  const spacemouse = function(controller)
    {
    if (controller.id.search("3Dconnexion") == -1)
      return;

    let x;
    let y;
    let z;
    let rot;
    let tiltX;
    let tiltY;

    // Left button
    checkButton(controller.buttons[0], controller.id, "spacemouse", "Left");

    // Right button
    checkButton(controller.buttons[1], controller.id, "spacemouse", "Right");

    // simulated buttons
    if (controller.buttonsSim === undefined)
      {
      controller.buttonsSim = {};
      controller.buttonsSim.xLeft = {};
      controller.buttonsSim.xLeft.pressed = 0;
      controller.buttonsSim.xRight = {};
      controller.buttonsSim.xRight.pressed = 0;
      controller.buttonsSim.yFore = {};
      controller.buttonsSim.yFore.pressed = 0;
      controller.buttonsSim.yBack = {};
      controller.buttonsSim.yBack.pressed = 0;
      controller.buttonsSim.zUp = {};
      controller.buttonsSim.zUp.pressed = 0;
      controller.buttonsSim.zDown = {};
      controller.buttonsSim.zDown.pressed = 0;
      };

    // x (- left, + right)
    x = controller.axes[0];
    if (x < -sensitivity || x > sensitivity)
      dispatchEvent(controller.id, "spacemouseMoveX", x);
    if (x == -1)
      controller.buttonsSim.xLeft.pressed = 1
    else
      controller.buttonsSim.xLeft.pressed = 0
    checkButton(controller.buttonsSim.xLeft, controller.id, "spacemouse",
                "Xleft");
    if (x == 1)
      controller.buttonsSim.xRight.pressed = 1
    else
      controller.buttonsSim.xRight.pressed = 0
    checkButton(controller.buttonsSim.xRight, controller.id, "spacemouse",
                "Xright");

    // y (- up, + down)
    y = controller.axes[1];
    if (y < -sensitivity || y > sensitivity)
      dispatchEvent(controller.id, "spacemouseMoveY", y);
    if (y == -1)
      controller.buttonsSim.yFore.pressed = 1
    else
      controller.buttonsSim.yFore.pressed = 0
    checkButton(controller.buttonsSim.yFore, controller.id, "spacemouse",
                "Yfore");
    if (y == 1)
      controller.buttonsSim.yBack.pressed = 1
    else
      controller.buttonsSim.yBack.pressed = 0
    checkButton(controller.buttonsSim.yBack, controller.id, "spacemouse",
                "Yback");

    // z (+ in, + out)
    z = controller.axes[2];
    if (z < -sensitivity || z > sensitivity)
      dispatchEvent(controller.id, "spacemouseMoveZ", z);
    if (z == -1)
      controller.buttonsSim.zUp.pressed = 1
    else
      controller.buttonsSim.zUp.pressed = 0
    checkButton(controller.buttonsSim.zUp, controller.id, "spacemouse",
                "Zup");
    if (z == 1)
      controller.buttonsSim.zDown.pressed = 1
    else
      controller.buttonsSim.zDown.pressed = 0
    checkButton(controller.buttonsSim.zDown, controller.id, "spacemouse",
                "Zdown");

    // rotate (+ clockwise, - counter-clockwise)
    rot = controller.axes[5];
    if (rot < -sensitivity || rot > sensitivity)
      dispatchEvent(controller.id, "spacemouseRotate", rot);

    // tilt (+ backward, - forward)
    tiltY = controller.axes[3];
    if (tiltY < -sensitivity || tiltY > sensitivity)
      dispatchEvent(controller.id, "spacemouseTiltY", tiltY);

    // tilt (+ left, - right)
    tiltX = controller.axes[4];
    if (tiltX < -sensitivity || tiltX > sensitivity)
      dispatchEvent(controller.id, "spacemouseTiltX", tiltX);
    };

  const xboxones = function(controller)
    {
    if (controller.id.search("X-Box One S") == -1)
      return;

    let lx;
    let ly;
    let lt;
    let rx;
    let ry;
    let rt;

    // A button
    checkButton(controller.buttons[0], controller.id, "xboxones", "A");

    // B button
    checkButton(controller.buttons[1], controller.id, "xboxones", "B");

    // X button
    checkButton(controller.buttons[2], controller.id, "xboxones", "X");

    // Y button
    checkButton(controller.buttons[3], controller.id, "xboxones", "Y");

    // Left Bumper button
    checkButton(controller.buttons[4], controller.id, "xboxones",
      "BumperLeft");

    // Right Bumper button
    checkButton(controller.buttons[5], controller.id, "xboxones",
      "BumperRight");

    // Back button
    checkButton(controller.buttons[6], controller.id, "xboxones", "Back");

    // Start button
    checkButton(controller.buttons[7], controller.id, "xboxones", "Start");

    // Power button
    checkButton(controller.buttons[8], controller.id, "xboxones", "Power");

    // LSB button
    checkButton(controller.buttons[9], controller.id, "xboxones", "LSB");

    // RSB button
    checkButton(controller.buttons[10], controller.id, "xboxones", "RSB");

    // LSB x
    lx = controller.axes[0];
    if (lx < -0.25 || lx > 0.25)
      dispatchEvent(controller.id, "xboxonesLSBx", lx);

    // LSB y
    ly = controller.axes[1];
    if (ly < -0.25 || ly > 0.25)
      dispatchEvent(controller.id, "xboxonesLSBy", ly);

    // Left Trigger
    // trigger's initial state is 0 when it should be -1
    lt = controller.axes[2];
    if (lt > -0.9 && lt != 0)
      dispatchEvent(controller.id, "xboxonesLSBt", lt);

    // RSB x
    rx = controller.axes[3];
    if (rx < -0.25 || rx > 0.25)
      dispatchEvent(controller.id, "xboxonesRSBx", rx);

    // RSB y
    ry = controller.axes[4];
    if (ry < -0.25 || ry > 0.25)
      dispatchEvent(controller.id, "xboxonesRSBy", ry);

    // Right Trigger
    // trigger's initial state is 0 when it should be -1
    rt = controller.axes[5];
    if (rt > -0.9 && rt != 0)
      dispatchEvent(controller.id, "xboxonesRT", rt);

    // DPAD
    if (controller.dpad === undefined)
      {
      controller.dpad = {};
      controller.dpad.left = {};
      controller.dpad.right = {};
      controller.dpad.up = {};
      controller.dpad.down = {};
      };
    controller.dpad.left.pressed = 0;
    controller.dpad.right.pressed = 0;
    controller.dpad.up.pressed = 0;
    controller.dpad.down.pressed = 0;

    if (controller.axes[6] == -1) // DPAD Left
      controller.dpad.left.pressed = 1;
    else if (controller.axes[6] == 1) // DPAD Right
      controller.dpad.right.pressed = 1;
    else if (controller.axes[7] == -1) // DPAD Up
      controller.dpad.up.pressed = 1;
    else if (controller.axes[7] == 1) // DPAD Down
      controller.dpad.down.pressed = 1;

    checkButton(controller.dpad.left, controller.id, "xboxones", "DPADleft");
    checkButton(controller.dpad.right, controller.id, "xboxones", "DPADright");
    checkButton(controller.dpad.up, controller.id, "xboxones", "DPADup");
    checkButton(controller.dpad.down, controller.id, "xboxones", "DPADdown");
    };

  const updateStatus = function()
    {
    //if (debug) console.dir("Gamepad.updateStatus()");

    if (focusonly && !isfocus)
      return;

    let index=0;
    let j;

    for (j in controllers)
      {
      spacemouse(controllers[j]);
      xboxones(controllers[j]);
      };

    requestAnimationFrame(updateStatus);
    };

  //
  // main
  //

  window.addEventListener("gamepadconnected", connecthandler);
  window.addEventListener("gamepaddisconnected", disconnecthandler);
  window.onblur = function() { isfocus=false; };
  window.onfocus = function()
    {
    isfocus=true;
    requestAnimationFrame(updateStatus);
    };

  setTimeout(scangamepads, 500);
  };

