
# Gamepad

A Javascript class for working with the HTML5 Gamepad API.  Supported
gamepads include the Microsoft X-Box controller and 3Dconnexion
SpaceMouse.

## Source

https://gitlab.com/lowswaplab/gamepad

## Copyright

Copyright © 2021-2022 Low SWaP Lab lowswaplab.com

## LICENSE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program.  If not, see
<https://www.gnu.org/licenses/>.

